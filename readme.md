1) Open a command prompt in the project's root directory (APM)

2) Type: `npm install`
    This installs the dependencies as defined in the package.json file.
    
3) Type: `npm start`
    This launches the TypeScript compiler (tsc) to compile the application and wait for changes. 
    It also starts the lite-server and launches the browser to run the application.

4) If using, visual studio code, use autosave afterDelay settings to save the modified code to autosave after little time. Goto, File->Preferences->User Settings and use the following code
 
* // Place your settings in this file to overwrite the default settings
* {
*     "files.exclude": {
*         "**/.git": true,
*         "**/.DS_Store": true
*     },
* 
*     //auto save 
*     "files.autoSave": "afterDelay"
* }
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, CanActivate } from '@angular/router';

import { AppComponent }  from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { ProductListComponent } from './products/product-list.component';
import { ProductFilterPipe } from './products/product-filter.pipe';
import { StarComponent } from './shared/star.component';
import { ProductDetailComponent } from './products/product-detail.componenet';
import { ProductDetailGuard } from './products/product-guard.service';

@NgModule({
  imports: [ 
    BrowserModule, 
    FormsModule,
    HttpModule,
    // this registers the router service provider; declares the router directive; exposes the configured routes
    // RouterModule,
    // but how will the router module know our configured routes; we pass them into the router modules by calling the forRoot() method
    RouterModule.forRoot([
      // the order of the routing paths matters; First Find First Select Basis working here
      { path: 'products', component: ProductListComponent },
      { path: 'product/:id', canActivate: [ProductDetailGuard], component: ProductDetailComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      
      // usually, this is used to handle 404 page not found.... but for now we will redirect to welcome page...
      { path: '**', redirectTo: 'welcome', pathMatch: 'full' },
      
    ])
  ],
  declarations: [ 
    AppComponent, 
    WelcomeComponent,
    ProductListComponent,
    ProductFilterPipe,
    StarComponent,
    ProductDetailComponent,
     ],
    
  providers: [ ProductDetailGuard ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }

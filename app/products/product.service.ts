import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

// just loads the operator map, do, catch without importing it....
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { IProduct } from './product';


@Injectable()
export class ProductService {

    private _productUrl = 'api/products/products.json';

    // angular to provide an instance to the http client service, so we identify the dependency in the constructor
    // private _http - angular will provide instance in this variable
    constructor (private _http : Http){   }

    getProducts(): Observable<IProduct[]> {
        return this._http.get(this._productUrl)
            // to convert the response url into json object of products
            // map raw http response into json object
            // <IProduct[]> - casting operator to cast the json object to array of products.
            .map((response: Response) => <IProduct[]>response.json())
            // to peak into the return response of the server without disrupting the flow;
            // useful to debugging
            //  
            .do( data => console.log('All: ') + JSON.stringify(data) )
            .catch(this.handleError);
    }

    getProduct(id: number): Observable<IProduct> {
    return this.getProducts()
        .map((products: IProduct[]) => products.find(p => p.productId === id));
    }

    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || 'Server error');

    }
}
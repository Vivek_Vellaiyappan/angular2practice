// definition of the guard in here using the service
// ActivatedRouteSnapshot - to deal with url parameters ; import from @angular/router
// @Injectable - since we are implementing a service, we use @Injectable here.
// Router instance - to redirect the page and load desired componenet
// CanActivate - guard method usage - guard to activate route ; import from @angular/router

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable()
export class ProductDetailGuard implements CanActivate {

    constructor ( private _route: Router ) { }

    canActivate(route: ActivatedRouteSnapshot) : boolean {
        let id = +route.url[1].path;
        if (isNaN(id) || id<1 ){
            alert('Invalid Product ID received!');
            this._route.navigate(['/products']);
            return false;
        }
        return true;
    }
}
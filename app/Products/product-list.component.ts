import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
    // selector used for nesting in another component
    selector: 'pm-products',
    moduleId: module.id,
    templateUrl: 'product-list.component.html',
    styleUrls: ['product-list.component.css']
})

export class ProductListComponent implements OnInit {

    pageTitle: string = 'Products List';
    imageWidth: number = 70;
    imageHeight: number = 40;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string = '';
    products: IProduct[];
    errorMessage: string;

    constructor (private _productService: ProductService){

    }
    
    ngOnInit(): void{
        console.log('Product Componenet Interface OnInit executed!..');
        this._productService.getProducts()
                this._productService.getProducts()
                    .subscribe(
                        products => this.products = products,
                        error => this.errorMessage = <any>error
                        // <any> is casting operator that casts any data from observable data into appropriate type
                    );

    }


    toggleImage() : void{
        this.showImage = !this.showImage
    }

    getMessage() : void{
    console.log("from products");
    }

  
    onRatingClicked(message: string):void{
        this.pageTitle = 'Product List ' + message;
    }

    /*
    products: IProduct[] = [
    {
        "productId": 1,
        "productName": "Leaf Rake",
        "productCode": "GDN-0011",
        "releaseDate": "March 19, 2016",
        "description": "Leaf rake with 48-inch wooden handle.",
        "price": 19.95,
        "starRating": 3.5,
        "imageUrl": "app/assets/images/logo.jpg"
        //"imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
    },
    {
        "productId": 2,
        "productName": "Garden Cart",
        "productCode": "GDN-0023",
        "releaseDate": "March 18, 2016",
        "description": "15 gallon capacity rolling garden cart",
        "price": 32.99,
        "starRating": 4.2,
        "imageUrl": "app/assets/images/logo.jpg"
        //"imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
    },

        {
        "productId": 8,
        "productName": "Saw",
        "productCode": "TBX-0022",
        "releaseDate": "May 15, 2016",
        "description": "15-inch steel blade hand saw",
        "price": 11.55,
        "starRating": 3.7,
        "imageUrl": "app/assets/images/logo.jpg"
//        "imageUrl": "http://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png"
    },
    {
        "productId": 10,
        "productName": "Video Game Controller",
        "productCode": "GMG-0042",
        "releaseDate": "October 15, 2015",
        "description": "Standard two-button video game controller",
        "price": 35.95,
        "starRating": 4.6,
        "imageUrl": "app/assets/images/logo.jpg"
//        "imageUrl": "http://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png"
    }
    ]; */

    


}